package org.electre.test.json;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class AppJackson
{
    public static void main(String[] args)
        throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jo = mapper.readTree(Constant.JSON);

        System.out.println(jo.get("firstName"));
        System.out.println(jo.get("address"));
        System.out.println(jo.get("address").get("city"));
        System.out.println(jo.get("city"));

        // Need the fields to be public or add
        // mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(Constant.PERSON);
        System.out.println(json);
    }
}
