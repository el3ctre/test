package org.electre.test.json;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class AppJsonApi
{
    public static void main( String[] args )
    {
        JsonReader jsonReader = Json.createReader(new StringReader(Constant.JSON));
        JsonObject jo = jsonReader.readObject();

        System.out.println(jo.get("firstName"));
        System.out.println(jo.get("address"));
        System.out.println(jo.getJsonObject("address").get("city"));
        System.out.println(jo.get("city"));
    }
}
