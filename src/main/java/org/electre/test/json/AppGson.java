package org.electre.test.json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class AppGson
{
    public static void main( String[] args )
    {
        JsonObject jo = new JsonParser().parse(Constant.JSON).getAsJsonObject();

        System.out.println(jo.get("firstName"));
        System.out.println(jo.get("address"));
        System.out.println(jo.getAsJsonObject("address").get("city"));
        System.out.println(jo.get("city"));

        Gson gson = new Gson();
        String json = gson.toJson(Constant.PERSON);
        System.out.println(json);
    }
}
