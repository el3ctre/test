package org.electre.test.json;

import org.json.JSONObject;

public class AppJsonOrg
{
    public static void main( String[] args )
    {
        JSONObject jo = new JSONObject(Constant.JSON);

        System.out.println(jo.get("firstName"));
        System.out.println(jo.get("address"));
        System.out.println(jo.getJSONObject("address").get("city"));
        try
        {
        System.out.println(jo.get("city"));
        }
        catch (Exception e)
        {
            System.err.println(e);
        }

        String json = new JSONObject(Constant.PERSON).toString();
        System.out.println(json);
    }
}
