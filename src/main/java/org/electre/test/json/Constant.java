package org.electre.test.json;

import java.util.ArrayList;
import java.util.List;

public class Constant
{

    public final static String JSON = "{\r\n" +
        "    \"firstName\": \"John\",\r\n" +
        "    \"lastName\": \"Smith\",\r\n" +
        "    \"age\": 25,\r\n" +
        "    \"address\": {\r\n" +
        "        \"streetAddress\": \"21 2nd Street\",\r\n" +
        "        \"city\": \"New York\",\r\n" +
        "        \"state\": \"NY\",\r\n" +
        "        \"postalCode\": 10021\r\n" +
        "    },\r\n" +
        "    \"phoneNumbers\": [\r\n" +
        "        {\r\n" +
        "            \"type\": \"home\",\r\n" +
        "            \"number\": \"212 555-1234\"\r\n" +
        "        },\r\n" +
        "        {\r\n" +
        "            \"type\": \"fax\",\r\n" +
        "            \"number\": \"646 555-4567\" \r\n" +
        "        }\r\n" +
        "    ] \r\n" +
        "}";

    public final static Person PERSON = constructPerson();

    public static Person constructPerson()
    {
        Person json = new Person();
        json.firstName = "John";
        json.lastName = "Smith";
        json.age = 25;
        Address address = new Address();
        address.streetAddress = "21 2nd Street";
        address.city = "New York";
        address.state = "NY";
        address.postalCode = 10021;
        json.address = address;
        List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
        PhoneNumber pn1 = new PhoneNumber();
        pn1.type = "home";
        pn1.number = "212 555-1234";
        phoneNumbers.add(pn1);
        PhoneNumber pn2 = new PhoneNumber();
        pn2.type = "fax";
        pn2.number = "646 555-4567";
        phoneNumbers.add(pn2);
        json.phoneNumbers = phoneNumbers;
        return json;
    }

    public static class Person
    {
        private String firstName;
        private String lastName;
        private int age;
        private Address address;
        private List<PhoneNumber> phoneNumbers;
        public String getFirstName()
        {
            return firstName;
        }
        public String getLastName()
        {
            return lastName;
        }
        public int getAge()
        {
            return age;
        }
        public Address getAddress()
        {
            return address;
        }
        public List<PhoneNumber> getPhoneNumbers()
        {
            return phoneNumbers;
        }
    }

    public static class Address
    {
        private String streetAddress;
        private String city;
        private String state;
        private int postalCode;
        public String getStreetAddress()
        {
            return streetAddress;
        }
        public String getCity()
        {
            return city;
        }
        public String getState()
        {
            return state;
        }
        public int getPostalCode()
        {
            return postalCode;
        }
    }

    public static class PhoneNumber
    {
        private String type;
        private String number;
        public String getType()
        {
            return type;
        }
        public String getNumber()
        {
            return number;
        }
    }

}
