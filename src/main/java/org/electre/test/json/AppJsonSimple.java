package org.electre.test.json;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class AppJsonSimple
{
    public static void main( String[] args )
    {
        Object obj = JSONValue.parse(Constant.JSON);
        JSONObject jo = (JSONObject) obj;

        System.out.println(jo.get("firstName"));
        System.out.println(jo.get("address"));
        System.out.println(((JSONObject) jo.get("address")).get("city"));
        System.out.println(jo.get("city"));

        String json = JSONValue.toJSONString(Constant.PERSON);
        System.out.println(json);
    }
}
